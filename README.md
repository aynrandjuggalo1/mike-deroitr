**Toy Programs by Mike DeRoitr**

## Descripton

Collected here is an assortment of toy programs written by Mike DeRoitr as he furthers his understanding of how to program & importantly, how the hell to use Git! You can reach Mike at aynrandjuggalo@gmail.com.

---

## Languages Used

Currently, there are programs in C, Ruby, sh, & COBOL (Mike took a course). Files w/o a suffix are either scripts or sample data for said scripts; inspecting the files will reveal which one.

---

By Mike DeRoitr.
